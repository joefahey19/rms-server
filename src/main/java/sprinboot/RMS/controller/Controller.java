package sprinboot.RMS.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sprinboot.RMS.model.Auth;
import sprinboot.RMS.model.Expense;
import sprinboot.RMS.model.Income;
import sprinboot.RMS.model.User;
import sprinboot.RMS.repository.ExpenseRepository;
import sprinboot.RMS.repository.IncomeRepository;
import sprinboot.RMS.repository.UserRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class Controller {

	@Autowired
	UserRepository repository;
	@Autowired
	IncomeRepository incomeRepository;
	@Autowired
	ExpenseRepository expenseRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	private List<User> users;

//USER CONTROLS//
	@PostMapping("/createUser")
	public User createUser(@RequestBody User user) {
		
		String forEncoding = user.getPassword();
		user.setPassword(passwordEncoder.encode(forEncoding));

		repository.save(user);

		return user;

	}
	
	@PostMapping("/authenticate")
	public boolean authUser(@RequestBody Auth authObject) {
		
	
	User user = authObject.getUser();
	String password = user.getPassword();
	String toVerify = authObject.getPassword();
	
	return passwordEncoder.matches(toVerify, password);

	}

	@PostMapping("/updateUserDetails")
	public User updateUserDetails(@RequestBody User user) {

		User updateMe = new User(); // new user obj
		int id = user.getId(); // get users id

		updateMe = repository.findById(id);
		updateMe.setFirstName(user.getFirstName());
		updateMe.setLastName(user.getLastName());
		updateMe.setJobTitle(user.getJobTitle());
		updateMe.setEmailAddress(user.getEmailAddress());
		repository.save(updateMe);
		return updateMe;

	}

	@GetMapping("/findall")
	public List<User> findAll() {
		List<User> users = repository.findAll();

		return users;
	}

	@RequestMapping("/")
	public String home() {
		return "Spring boot active!";
	}

	@RequestMapping("/users")
	public List<User> getUsers() {
		return users;
	}

	@PostMapping("/findById")
	public User findUserById(@RequestBody int id) {

		User upToDateUser = new User();
		upToDateUser = repository.findById(id);

		return upToDateUser;

	}

//INCOME CONTROLS//
	@PostMapping("/createIncome")
	public int createIncome(@RequestBody Income income) {

		incomeRepository.save(income); // saves income object to the db
		updateIncome(income);
		return income.getId();

	}

	public void updateIncome(Income income) {

		User user = new User();

		int invoiceValue = income.getInvoiceValue(); // get new invoice value
		user = repository.findById(income.getUserId()); // find user & table in db

		double currentIncome = user.getIncome(); // retrieve current total income for user
		double newIncome = currentIncome + invoiceValue;
//		
		user.setIncome(newIncome); // set user total income to be updated value
		repository.save(user); // update backend
//	
	}

	@PostMapping("/incomeSuccess")
	public Income incomeSuccess(@RequestBody int id) {

		Income income = incomeRepository.findById(id);

		return income;
	}

	// Return all invoices for a user
	@PostMapping("/incomeSummary")
	public List<Income> incomeSummary(@RequestBody int userId) {

		List<Income> userInvoices = incomeRepository.findAllByUserId(userId);

		return userInvoices;
	}

	@PostMapping("/deleteIncome")
	public Income deleteIncome(@RequestBody int id) {
		
		Income income = incomeRepository.findById(id);
		int userId = income.getUserId();
		reduceIncome(userId, id);
		
		incomeRepository.delete(income);
		return income;
	}
	
	public void reduceIncome(int userId, int id) {

		User user = repository.findById(userId);
		Income incomeObj = incomeRepository.findById(id);

		double income = user.getIncome();
		double incValue = incomeObj.getInvoiceValue();

		income = income - incValue;

		user.setIncome(income);
		repository.save(user);

	}

	@PostMapping("/getInvoiceById")
	public Income findInvoiceById(@RequestBody int id) {

		return incomeRepository.findById(id);
	}

	@PostMapping("/updateInvoiceDetails")
	public Income updateIncomeDetails(@RequestBody Income income) {

		Income updateMe = new Income(); // new user obj
		Income originalIncome = new Income();
		int id = income.getId(); // get users id
		
		originalIncome = incomeRepository.findById(id);
		incomeInvoiceUpdated(originalIncome, income);

		updateMe = incomeRepository.findById(id);
		updateMe.setClientName(income.getClientName());
		updateMe.setDateOfInvoice(income.getDateOfInvoice());
		updateMe.setInvoiceValue(income.getInvoiceValue());
		updateMe.setNotes(income.getNotes());
		updateMe.setPaymentReceived(income.isPaymentReceived());
		updateMe.setDateOfPayment(income.getDateOfPayment());
		incomeRepository.save(updateMe);
		return updateMe;

	}
	
	public void incomeInvoiceUpdated(Income originalIncome, Income newIncome) {
		
		User user = new User();
		user = repository.findById(originalIncome.getUserId());
		double incomeTotal = user.getIncome();
		
		incomeTotal = incomeTotal - originalIncome.getInvoiceValue();
		incomeTotal = incomeTotal + newIncome.getInvoiceValue();
		user.setIncome(incomeTotal);
		repository.save(user);
		
	}


//EXPENSE CONTROLS//
	@PostMapping("/createExpense")
	public int createExpense(@RequestBody Expense expense) {

		expenseRepository.save(expense);
		updateExpense(expense);
		return expense.getId();

	}

	public void updateExpense(Expense expense) {

		User user = new User();

		int expenseValue = expense.getExpenseValue(); // get new expense value
		user = repository.findById(expense.getUserId()); // find user & table in db

		double currentOutgoing = user.getOutgoing(); // retrieve current total income for user
		double newOutgoing = currentOutgoing + expenseValue;

		user.setOutgoing(newOutgoing); // set user total income to be updated value
		repository.save(user); // update backend

	}

	public void outgoingExpenseUpdated(Expense originalExpense, Expense newExpense) {

		User user = new User();
		user = repository.findById(originalExpense.getUserId());
		double outgoingTotal = user.getOutgoing();
		outgoingTotal = outgoingTotal - originalExpense.getExpenseValue();
		outgoingTotal = outgoingTotal + newExpense.getExpenseValue();
		user.setOutgoing(outgoingTotal);
		repository.save(user);

	}

	@PostMapping("/getExpenseById")
	public Expense findExpenseById(@RequestBody int id) {

		return expenseRepository.findById(id);
	}

	@PostMapping("/updateExpenseDetails")
	public Expense updateExpenseDetails(@RequestBody Expense expense) {

		Expense updateMe = new Expense(); // new expense obj
		Expense originalExpense = new Expense();
		int id = expense.getId(); // get expense id

		originalExpense = expenseRepository.findById(id);
		outgoingExpenseUpdated(originalExpense, expense);

		updateMe = expenseRepository.findById(id);
		updateMe.setExpenseType(expense.getExpenseType());
		updateMe.setDateOfExpense(expense.getDateOfExpense());
		updateMe.setExpenseValue(expense.getExpenseValue());
		updateMe.setNotes(expense.getNotes());
		expenseRepository.save(updateMe);
		return updateMe;

	}

	// Return all expenses for a user
	@PostMapping("/expenseSummary")
	public List<Expense> expenseSummary(@RequestBody int userId) {

		List<Expense> userExpenses = expenseRepository.findByUserId(userId);

		return userExpenses;
	}

	@PostMapping("/deleteExpense")
	public Expense deleteExpense(@RequestBody int id) {

		Expense expense = expenseRepository.findById(id);
		int userId = expense.getUserId();
		reduceOutgoing(userId, id);

		expenseRepository.delete(expense);
		return expense;
	}

	public void reduceOutgoing(int userId, int id) {

		User user = repository.findById(userId);
		Expense expense = expenseRepository.findById(id);

		double outgoing = user.getOutgoing();
		double expValue = expense.getExpenseValue();

		outgoing = outgoing - expValue;

		user.setOutgoing(outgoing);
		repository.save(user);

	}

}
