package sprinboot.RMS.controller;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.mail.smtp.SMTPTransport;

import sprinboot.RMS.model.ContactEmail;
import sprinboot.RMS.model.RegisterEmail;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class EmailController {
    
    @PostMapping("/register")
    public static void SendRegisterEmailSMTP(@RequestBody RegisterEmail emailObject) {
    	
        final String SMTP_SERVER = "smtp.gmail.com";
        final String USERNAME = "revmansys@gmail.com";
        final String PASSWORD = "RevenueSystem1!";

        final String EMAIL_FROM = "joefahey19@gmail.com";
        final String EMAIL_TO = emailObject.getEmailAddress();
        final String EMAIL_TO_CC = "";

        final String EMAIL_SUBJECT = "Welcome to RMS!";
        final String EMAIL_TEXT = 	"Welcome to the next stage in your business management, " + emailObject.getfName() + 
        							"\n Have a look around your dashboard, add some invoices and expenses, get to know how we work. \n  "
        							+ "If you have any questions, give us a shout. \n All the best! \n The RMS Team";
   	 	Properties prop = System.getProperties();
        
        prop.setProperty("mail.transport.protocol", "smtp");     
        prop.setProperty("mail.host", "smtp.gmail.com");  
        prop.put("mail.smtp.auth", "true");  
        prop.put("mail.smtp.port", "587");  
        prop.put("mail.debug", "true");  
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.socketFactory.port", "587");  
        prop.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
        prop.put("mail.smtp.socketFactory.fallback", "true");

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {
		
			// from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

			// to 
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

			// cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

			// subject
            msg.setSubject(EMAIL_SUBJECT);
			
			// content 
            msg.setText(EMAIL_TEXT);
			
            msg.setSentDate(new Date());

			// Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
			
			// connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
			
			// send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
   }
    
    @PostMapping("/contactEmail")
    public static void SendEmailSMTP(@RequestBody ContactEmail contactEmail) {
    	
        final String SMTP_SERVER = "smtp.gmail.com";
        final String USERNAME = "revmansys@gmail.com";
        final String PASSWORD = "RevenueSystem1!";

        final String EMAIL_FROM = "revmansys@gmail.com";
        final String EMAIL_TO = "x18133720@student.ncirl.ie";
        final String EMAIL_TO_CC = "";

        final String EMAIL_SUBJECT = "New Contact Us Form Received!";
        final String EMAIL_TEXT = 	"New Message: \n From: " + contactEmail.getName() + "\n Email: " + contactEmail.getEmail() + 
        							"\n Phone Number: " + contactEmail.getPhone() + "\n Message: " + contactEmail.getMessage();
   	 	Properties prop = System.getProperties();
        
        prop.setProperty("mail.transport.protocol", "smtp");     
        prop.setProperty("mail.host", "smtp.gmail.com");  
        prop.put("mail.smtp.auth", "true");  
        prop.put("mail.smtp.port", "587");  
        prop.put("mail.debug", "true");  
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.socketFactory.port", "587");  
        prop.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
        prop.put("mail.smtp.socketFactory.fallback", "true");

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {
		
			// from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

			// to 
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

			// cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

			// subject
            msg.setSubject(EMAIL_SUBJECT);
			
			// content 
            msg.setText(EMAIL_TEXT);
			
            msg.setSentDate(new Date());

			// Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
			
			// connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
			
			// send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
   }

}
