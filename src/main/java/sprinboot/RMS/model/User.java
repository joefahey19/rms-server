package sprinboot.RMS.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User implements Serializable {
	

	private static final long serialVersionUID = -2343243243242432341L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "jobTitle")
	private String jobTitle;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "emailAddress")
	private String emailAddress;
	
	@Column(name = "active")
	private boolean isActive;
	
	@Column(name = "admin")
	private boolean isAdmin;
	
	@Column(name = "income")
	private double income;
	
	@Column (name = "outgoing")
	private double outgoing;
	
	@Column (name = "dateOfBirth")
	private String dateOfBirth;
	
	@Column (name = "taxNo")
	private String taxNo;
	
	public User() {

	}

	public User(String firstName, String lastName, String jobTitle, String password,
			String emailAddress, boolean isActive, boolean isAdmin, double income, double outgoing, String dateOfBirth, String taxNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.jobTitle = jobTitle;
		this.password = password;
		this.emailAddress = emailAddress;
		this.isActive = isActive;
		this.isAdmin = isAdmin;
		this.income = income;
		this.outgoing = outgoing;
		this.dateOfBirth = dateOfBirth;
		this.taxNo = taxNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getTaxNo() {
		return taxNo;
	}
	
	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", jobTitle=" + jobTitle + ", password=" + password + ", emailAddress=" + emailAddress
				+ ", isActive=" + isActive + ", isAdmin=" + isAdmin + ", income=" + income + ", outgoing=" + outgoing
				+ ", dateOfBirth=" + dateOfBirth + ", taxNo=" + taxNo +  "]";
	}

	public double getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(double outgoing) {
		this.outgoing = outgoing;
	}
	
	

	
}