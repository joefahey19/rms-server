package sprinboot.RMS.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "expenses")
public class Expense implements Serializable{
	
	private static final long serialVersionUID = -2343243243242432341L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "expenseType")
	private String expenseType;
	
	@Column(name = "userId")
	private int userId;
	
	@Column(name = "expenseValue")
	private int expenseValue;
	
	@Column(name = "dateOfExpense")
	private String dateOfExpense;
	
	@Column(name = "notes")
	private String notes;
	
	public Expense() {
		
	}

	public Expense(String expenseType, int userId, int expenseValue, String dateOfExpense, String notes) {
		super();
		this.expenseType = expenseType;
		this.userId = userId;
		this.expenseValue = expenseValue;
		this.dateOfExpense = dateOfExpense;
		this.notes = notes;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getExpenseValue() {
		return expenseValue;
	}

	public void setExpenseValue(int expenseValue) {
		this.expenseValue = expenseValue;
	}

	public String getDateOfExpense() {
		return dateOfExpense;
	}

	public void setDateOfExpense(String dateOfExpense) {
		this.dateOfExpense = dateOfExpense;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "expenseType=" + expenseType + ", userId=" + userId + ", expenseValue="
				+ expenseValue + ", dateOfExpense=" + dateOfExpense + ", notes=" + notes + "]";
	}

}

