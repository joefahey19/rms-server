package sprinboot.RMS.model;

public class RegisterEmail {
	
	private String emailAddress;
	private String fName;
	
	
	public RegisterEmail() {
		
	}
	
	public RegisterEmail(String emailAddress, String fName) {
		super();
		this.emailAddress = emailAddress;
		this.fName = fName;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	
	

}
