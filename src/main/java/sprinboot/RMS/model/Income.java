package sprinboot.RMS.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "income")
public class Income implements Serializable{
	
	private static final long serialVersionUID = -2343243243242432341L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "clientName")
	private String clientName;
	
	@Column(name = "userId")
	private int userId;
	
	@Column(name = "invoiceValue")
	private int invoiceValue;
	
	@Column(name = "dateOfInvoice")
	private String dateOfInvoice;
	
	@Column(name = "paymentReceived")
	private boolean paymentReceived;
	
	@Column(name = "dateOfPayment")
	private String dateOfPayment;
	
	@Column(name = "notes")
	private String notes;
	
	public Income() {
		
	}
	
	public Income(String clientName, int userId, int invoiceValue, String dateOfInvoice,
			boolean paymentReceived, String dateOfPayment, String notes) {
		super();
		this.clientName = clientName;
		this.userId = userId;
		this.invoiceValue = invoiceValue;
		this.dateOfInvoice = dateOfInvoice;
		this.paymentReceived = paymentReceived;
		this.dateOfPayment = dateOfPayment;
		this.notes = notes;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(int invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public String getDateOfInvoice() {
		return dateOfInvoice;
	}

	public void setDateOfInvoice(String dateOfInvoice) {
		this.dateOfInvoice = dateOfInvoice;
	}

	public boolean isPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(boolean paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public String getDateOfPayment() {
		return dateOfPayment;
	}

	public void setDateOfPayment(String dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Income [id=" + id + ", clientName=" + clientName + ", userId=" + userId + ", invoiceValue="
				+ invoiceValue + ", dateOfInvoice=" + dateOfInvoice + ", paymentReceived=" + paymentReceived
				+ ", dateOfPayment=" + dateOfPayment + ", notes=" + notes + "]";
	}

}
