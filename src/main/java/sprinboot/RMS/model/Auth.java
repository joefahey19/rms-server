package sprinboot.RMS.model;

public class Auth {

	private User user;
	private String password;
	
	public Auth() {
		
	}
	
	public Auth(User user, String password) {
		super();
		this.user = user;
		this.password = password;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
