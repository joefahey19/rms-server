package sprinboot.RMS.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sprinboot.RMS.model.Income;

@Repository
public interface IncomeRepository extends CrudRepository<Income, Long> {
	
	List<Income> findAll();
	List<Income> findByClientName(String clientName);
	Income findById(int id);
	List<Income> findAllByUserId(int userId);

}
