package sprinboot.RMS.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sprinboot.RMS.model.Expense;

@Repository
public interface ExpenseRepository extends CrudRepository<Expense, Long> {
	
	List<Expense> findAll();
	List<Expense> findByExpenseType(String expenseType);
	List<Expense> findByUserId(int userId);
	Expense findById(int id);

}

