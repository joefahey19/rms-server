package sprinboot.RMS.repository;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sprinboot.RMS.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

	List<User> findAll();
	User findById(int id);
}
